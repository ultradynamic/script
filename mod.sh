#! /bin/bash
mkdir -p /tmp/random_mod
cd /tmp/random_mod
wget "https://modarchive.org/index.php?request=view_player&query=random&autoplay" -O mod_page
mod_url=$(grep "https://api.modarchive.org/downloads.php?" mod_page | cut -d \" -f 2)
rm mod_page
mod_name=$(echo $mod_url | cut -d \# -f 2)
echo "$mod_name"
wget $mod_url -O $mod_name
ocp -p $mod_name
