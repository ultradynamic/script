cat ~/script/m11x/battery.art
echo
echo " $(cpupower frequency-info -p)"
echo
echo " select performance mode: "
echo "  [0] = power save"
echo "  [1] = on-demand"
echo "  [2] = performance"
echo "  [3] = schedutil"
echo
read mode

if [ "$mode" == "0" ]; then
  governor=powersave
elif [ "$mode" == "1" ]; then
  governor=ondemand
elif [ "$mode" == "2" ]; then
  governor=performance
elif [ "$mode" == "3" ]; then
  governor=schedutil
fi

echo " setting performance to $governor"
sudo cpupower frequency-set -g $governor
rc=$?
if [[ $rc == 0 ]]; then
  notify-send -u normal "performance" "set to [$governor]"
else
  echo "an error occurred, press any key to continue"
  read
fi
