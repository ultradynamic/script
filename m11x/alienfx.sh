TOGGLE=$HOME/.alienfx-on

if [ ! -e $TOGGLE ]; then
  touch $TOGGLE
  notify-send -u low "AlienFX" "going dark..."
  alienfx
else
  rm $TOGGLE
  notify-send -u low "AlienFX" "activating..."
  ~/script/m11x/alienfx-scheme1.sh
fi
