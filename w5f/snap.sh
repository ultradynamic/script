DATE=`date +"%Y%m-%d-%H:%m:%s"`
if [ ! -d "$HOME/webcam" ]; then
  mkdir $HOME/webcam
fi
LD_PRELOAD=/usr/lib/libv4l/v4l2convert.so fswebcam -r 384x288 --quiet --save ~/webcam/img-$DATE.jpg
notify-send -u normal 'webcam ' "captured [<i>img-$DATE.jpg</i>]"
