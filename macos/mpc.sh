#! /bin/bash
if [ "$1" != "" ]; then
    result=$(/usr/local/bin/mpc $1 -f  "%artist% - %album% subtitle: %track% \\ %title% status:" 2>&1)
    
    if [ "$result" == "mpd error: Connection refused" ]; then
        /usr/local/bin/terminal-notifier -title MPD -message "service is not running" -group MPD -timeout 2
    else 
        if [[ $result == *"playing"* || $result == *"paused"* ]]; then
            title=${result%subtitle:*}
            subtitle=${result%status:*}
            subtitle=${subtitle#*subtitle:}
            message=${result#*status:}
            message=${message%volume:*}
            message=${message/[}
            message=${message/]}

            /usr/local/bin/terminal-notifier -title "$subtitle" -subtitle "$title" -message "$message" -group MPD -timeout 2
        else 
            /usr/local/bin/terminal-notifier -title MPD -message "player is stopped" -group MPD -timeout 2
        fi

    fi
else
    echo "need an operation to pass to mpc"
fi
