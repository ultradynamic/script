#!/bin/bash
card=1
warn_cap=10
off_cap=5

acpi -b | awk -F'[,:%]' '{print $2, $3}' | {
	read -r bat0_status bat0_capacity
	read -r bat1_status bat1_capacity

	if [[ ("$bat0_status" == Discharging || "$bat1_status" = Discharging) && ("$bat0_capacity" -lt $warn_cap && "$bat1_capacity" -lt $warn_cap)]]; then
			if [[ ("$bat0_capacity" -lt $off_cap && "$bat1_capacity" -lt $off_cap) ]]; then
			    amixer -q -c $card set 'Beep' 20% unmute && amixer -q -c $card set 'Loopback Mixing' 'Enabled' && beep -f 400 -l 200 -r 3 && amixer -q -c $card set 'Loopback Mixing' 'Disabled' && amixer -q -c $card set 'Beep' 0% mute
				notify-send '[!] low battery' 'suspending system...' -u critical
				logger "critical battery threshold - suspending"
				sleep 5s
				systemctl suspend
			else
				notify-send '[!] low battery' 'connect power adapter' -u normal
				amixer -q -c $card set 'Beep' 40% unmute && amixer -q -c $card set 'Loopback Mixing' 'Enabled' && beep -f 4000 -l 100 -r 5 && amixer -q -c $card set 'Loopback Mixing' 'Disabled' && amixer -q -c $card set 'Beep' 0% mute
		fi
	fi
}
