#!/bin/bash
card=1
sink=0
pactl set-sink-mute $sink toggle && notify-send -u low 'volume' $(amixer -c $card get Master | grep 'Mono: Playback' | awk '{print $6}')
