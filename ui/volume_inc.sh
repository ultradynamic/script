#!/bin/bash
card=1

amixer -c $card get Master -M | grep -oE \[\[:digit:\]\]* | {
	read -r volume

	if [[ ("$(amixer -c $card get Master -M |  grep -oE \[\[:digit:\]\]*%)" != "100%")]]; then
			pactl set-sink-volume @DEFAULT_SINK@ +5%
			sleep 1.0
			notify-send -u low 'volume' -h int:value:$(amixer -c $card get Master -M |  grep -oE \[\[:digit:\]\]*%)
		else 
			sleep 1.0
			notify-send -u low 'volume' -h int:value:$(amixer -c $card get Master -M |  grep -oE \[\[:digit:\]\]*%)
	fi
}
