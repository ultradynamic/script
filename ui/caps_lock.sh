state="`xset -q | grep Caps | awk '{print $4}'`"

if [[($state == "on")]]; then
	notify-send "caps lock" "$state" -u low
fi
