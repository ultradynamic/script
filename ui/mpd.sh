#! /bin/bash
if [ "$1" != "" ]; then
    result=$(/usr/bin/mpc $1 -f  "%artist% <i>%album%</i>subtitle:\[%track%\] %title% status:" 2>&1)
    
    if [ "$result" == "mpd error: Connection refused" ]; then
        /usr/bin/notify-send -title MPD -message "service is not running" -group MPD -timeout 2
    else 
        if [[ $result == *"playing"* || $result == *"paused"* ]]; then
            title=${result%subtitle:*}
            subtitle=${result%status:*}
            subtitle=${subtitle#*subtitle:}
            message=${result#*status:}
            message=${message%volume:*}

            /usr/bin/notify-send -u low "$title" "$subtitle$message"
        else 
            /usr/bin/notify-send -u low "MPD" "player is stopped"
        fi

    fi
else
    echo "need an operation to pass to mpc"
fi
